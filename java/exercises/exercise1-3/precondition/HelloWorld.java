public class HelloWorld {

    private static String message = "World! Hello!";

    public static void main(String[] args) {
        System.out.println(getMessage());
    }

    private static String getMessage() {
        return message;
    }
}
