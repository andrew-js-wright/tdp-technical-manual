package vehicle.service.resource;

import org.junit.Before;
import org.junit.Test;
import vehicle.api.Vehicle;

import static org.junit.Assert.assertEquals;

public class VehicleResourceTest {
    private VehicleResource resource;

    @Before
    public void setup(){
        resource = new VehicleResource();
    }

    @Test
    public void getVehiclesReturnsTheExpectedRegistration() {
        final String expectedRegistration = "ABC123";

        Vehicle actualVehicle = resource.getVehicles().get(0);

        assertEquals(expectedRegistration, actualVehicle.registration);
    }
}
