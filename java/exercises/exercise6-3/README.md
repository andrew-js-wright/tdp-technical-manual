# Write an integration test
## Set up the structure
In the same way we did with `vehicle-service` set up the client directory structure
so there is a test section of the project.

## Add the test dependencies
Again, the same as the vehicle-service add the correct gradle dependencies and make
them appear in IntelliJ

## Create the test file
Enter the following into `VehicleIntegrationTests`:

```
package vehicle.client;

import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.Before;

public class VehicleIntegrationTests {
    private VehicleClient client;

    @Before
    public void setup() {
        JerseyClient jerseyClient = JerseyClientBuilder.createClient();
        client = new VehicleClient(jerseyClient, "http://localhost:8080");
    }
}
```

This will create a jersey client and set the base path. Lets use the client now
to test the service:

```
    @Test
    public void testGetVehiclesReturnsCorrectVehicle() {
        List<Vehicle> actualVehicleList = client.getVehicles();
        assertEquals(actualVehicleList.get(0).registration, "ABC123");
        assertEquals(actualVehicleList.size(), 1);
    }
```

## Run it
Ensure that the service is running in the back ground and then run the integration
tests.

You should see some console statements appear in the running service to prove
that the tests have reached it like follows:
```
127.0.0.1 - - [18/Apr/2016:10:42:02 +0000] "GET /vehicles HTTP/1.1" 200 50 "-" "Jersey/2.22.1 (HttpUrlConnection 1.8.0_77)" 123
```
