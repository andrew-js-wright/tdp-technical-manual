Packaging it up
---------------
Now we have a Java program with more than one file you can see it would be difficult
to distribute this among your peers never mind promoting it through various environments.

Java Archives (JAR files)

Java solves the problem of distributing large programs by providing a file format
which compresses the files and launches them. Lets first have a look at how to do this
on the command line.

```
jar cf Vehicle.jar .
```

We can now use this from other programs. For example, lets create a driver application.

In this driver application we hold an association from Drivers to Vehicles.

1. Create a new directory called 'DriverApp`
2. In here lets add a folder of libraries called lib
3. Copy the Vehicle.jar into your lib directory
4. Create a new file called Driver.java

```
package driver.api;

import vehicle.api.Vehicle;

public class Driver {
    public Vehicle vehicle;
    public String name;
    public int age;
    public String address;
    public boolean banned = false;

    public String toString() {
        String driver = " { vehicle: " + vehicle.toString();
        driver += ", name: " + name;
        driver += ", age: " + age;
        driver += ", address: " + address;
        driver += ", banned: " + banned;
        driver += "}";
        return driver;
    }
}

```

5. Now lets use driver from a class called `Application`
```
import driver.api.Driver;
import vehicle.api.Vehicle;

public class Application {
    public static void main(String args[]) {
        Vehicle vehicle = new Vehicle();
        vehicle.id = 0;
        vehicle.registration = "ABC123";
        vehicle.colour = "Blue";

        Driver driver = new Driver();
        driver.vehicle = vehicle;
        driver.name = "Laura Smithson";
        driver.address = "10 Street, Town, Country";

        System.out.println(driver);
    }
}
```

6. Run `javac Application.java`
What happens now?? Why does this happen?

Java Class Path
- The class path is where Java looks when a class is referenced in source.
- The fact that we are getting an error suggesting the Vehicle class can't be found
suggests that it's not on the class path so having it in the same directory wasn't enough.

7. Run `javac -cp .:libs/Vehicle.jar Application.java`
8. Run `java -cp .:libs/Vehicle.jar Application.java`
