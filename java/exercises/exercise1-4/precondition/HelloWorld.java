import vehicle.api.Vehicle;

public class HelloWorld {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        vehicle.id = 0;
        vehicle.registration = "ABC123";
        vehicle.colour = "Blue";

        System.out.println(vehicle);
    }
}
