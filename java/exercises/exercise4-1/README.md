# Dropwizard - extract API

We have created an API - Application Public Interace i.e. a Vehicle.
We're going to create a service which uses this interface. There is an
important distingition to be made here:

The service is a different project from the API because the API can
be used by many different projects whereas the service will be a stand
alone entity.

So lets reflect this in our directory structure:

```
├── vehicle-api
│   ├── build.gradle
│   └── src
│       └── main
│           ├── java
│           │   ├── HelloWorld.java
│           │   └── vehicle
│           │       └── api
│           │           └── Vehicle.java
│           └── main.iml
└── vehicle-service
    └── src
        └── main
            └── java
                └── vehicle
                    └── service
```

## Create a multi project build
When we compile the project now we will want the whole project (API and service)
to be compiled and built. To do this all we need to do is create a file called
`settings.gradle` in the root directory with the following contents:

```
include 'vehicle-api', 'vehicle-service'
```

Now run:
```
gralde clean build
```

Notice that the build output now says 3 projects are being built and the API
project is being built using the same build.gradle file as before.

## Register the vehicle service as a Java project
In the same way we did for the API let gradle know that the `vehicle-service` module
is a java project.

## Generate files for IntelliJ
Out with the old and in with the new:
```
gradle cleanIdea idea
```

1. Open IntelliJ
2. Import project
3. Navigate to root project location
4. Select `import project from external model`
5. Select gradle as the external model
6. Select to use local gradle distribution
7. Set path to `/usr/local/opt/gradle/libexec`
8. Pick 1.8 as the Gradle JVM
9. Click Finish
