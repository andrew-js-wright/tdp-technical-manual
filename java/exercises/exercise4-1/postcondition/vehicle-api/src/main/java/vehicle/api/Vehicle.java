package vehicle.api;

public class Vehicle {
    public int id;
    public String registration;
    public String colour;

    @Override
    public String toString() {
        String vehicle = "{ id: " + id;
        vehicle += ", registration: " + registration;
        vehicle += ", colour: " + colour + " }";
        return vehicle;
    }
}
