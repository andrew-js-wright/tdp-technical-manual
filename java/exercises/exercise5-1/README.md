# Creating a test
Update the dependencies section of the vehicl-service/build.gradle file to
match the following:
```
dependencies {
    compile (
        'io.dropwizard:dropwizard-core:0.9.2',
        project(':vehicle-api')
    )
    testCompile (
        'io.dropwizard:dropwizard-testing:0.9.2'
     )
}

and refresh the gradle depenendecies in IntelliJ.

```
Create the following directory structure within the vehicle-service folder:
```
test/java/vehicle/service/resource
```
In the resource folder create a class called `VehicleResourceTest`

Make that class look like the following:
```
package vehicle.service.resource;

import org.junit.Before;
import org.junit.Test;
import vehicle.api.Vehicle;

import static org.junit.Assert.assertEquals;

public class VehicleResourceTest {
        private VehicleResource resource;

        @Before
        public void setup(){
            resource = new VehicleResource();
        }

        @Test
        public void getVehiclesReturnsTheExpectedRegistration() {
            final String expectedRegistration = "ABC123";

            Vehicle actualVehicle = resource.getVehicles().get(0);

            assertEquals(expectedRegistration, actualVehicle.registration);
        }
}
```

Now click the green triangles next to the method names or the double triangle next to
the class to run all of the tests. You should see these being run in IntelliJ

Add any more test cases you think are required.
