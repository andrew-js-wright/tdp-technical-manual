package vehicle.service;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import vehicle.api.Vehicle;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VehicleDaoMapper implements ResultSetMapper<Vehicle> {
    public Vehicle map(int index, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        Vehicle vehicle = new Vehicle();
        vehicle.id = resultSet.getInt("id");
        vehicle.registration = resultSet.getString("registration");
        vehicle.colour = resultSet.getString("colour");
        return vehicle;
    }
}
