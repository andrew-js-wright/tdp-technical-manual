package vehicle.service.resource;

import vehicle.api.Vehicle;
import vehicle.service.VehicleDao;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/vehicles")
@Produces(MediaType.APPLICATION_JSON)
public class VehicleResource {
    private VehicleDao vehicleDao;

    public VehicleResource(VehicleDao dao) {
        this.vehicleDao = dao;
    }

    @GET
    public List<Vehicle> getVehicles() {
        return vehicleDao.getVehicles();
    }
}

