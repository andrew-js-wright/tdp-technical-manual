package vehicle.service;

import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;
import vehicle.service.resource.VehicleResource;

public class VehicleServiceApplication extends Application<VehicleServiceConfiguration> {

    public static void main(String[] args) throws Exception {
        new VehicleServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "vehicle-service";
    }

    @Override
    public void run(VehicleServiceConfiguration configuration,
                    Environment environment) {

        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");

        final VehicleDao vehicleDao = jdbi.onDemand(VehicleDao.class);

        final VehicleResource resource = new VehicleResource(vehicleDao);
        environment.jersey().register(resource);
    }
}
