SELECT vehicle.id, registration, colour_lookup.name as colour
FROM vehicle
JOIN colour_lookup ON vehicle.primary_colour_id = colour_lookup.id
LIMIT 10;