package vehicle.client;

import vehicle.api.Vehicle;

import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class VehicleIntegrationTests {
    private VehicleClient client;

    @Before
    public void setup() {
        JerseyClient jerseyClient = JerseyClientBuilder.createClient();
        client = new VehicleClient(jerseyClient, "http://localhost:8080");
    }

    @Test
    public void testGetVehiclesReturnsCorrectVehicle() {
        List<Vehicle> actualVehicleList = client.getVehicles();
        assertEquals(actualVehicleList.get(0).registration, "ABC123");
        assertEquals(actualVehicleList.size(), 1);
    }
}
