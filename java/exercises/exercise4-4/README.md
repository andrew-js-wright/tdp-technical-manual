# Add get vehicles endpoint

Now we want to expose a URL from our service which will allow us to access a list of vehicles.
To do this we can use a Resource which maps a web URL to a java method and used Jackson
to map Java objects to JSON objects. Which can be consumed by unrelated clients. 

## Create VehicleResource

1. Create a package called `vehicle.service.resources`
2. Create a class in that package called VehicleResource
3. Copy the following into that class:
```
package vehicle.resource;

import vehicle.api.Vehicle;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/vehicles")
@Produces(MediaType.APPLICATION_JSON)
public class VehicleResource {
    private List<Vehicle> vehicles;

    public VehicleResource() {
        Vehicle vehicle = new Vehicle();
        vehicle.id = 0;
        vehicle.registration = "ABC123";
        vehicle.colour = "Blue";

        this.vehicles = new ArrayList<>();
        this.vehicles.add(vehicle);
    }

    @GET
    public List<Vehicle> getVehicles() {
        return vehicles;
    }
}
```

## Register the Resource

Once you have added the resource you will need to "Register" it. This is how
DropWizard knows it exists and it will provide that path to the webserver.

In your `VehicleServiceApplication` add the following to the body of the `run` method:
```
    final VehicleResource resource = new VehicleResource();
    environment.jersey().register(resource);
```

## Run the Service
In the project explorer right click the `VehicleServiceApplication` and select 
`Run 'VehicleServiceApplication.main()'`

This will give the following error:
```
usage: java -jar project.jar [-h] [-v] {server,check} ...

positional arguments:
    {server,check}         available commands

optional arguments:
    -h, --help             show this help message and exit
    -v, --version          show the application version and exit
```

In order to rectify this you'll need to modify the run configuration.

1. Beside the green play button at the top you should see a drop down. Click that
and select `Edit Configurations...`
2. In the text box labeled `Program arguments` enter `server`

## Talk to the service
Now the service is running on your localhost attached to port 8080.

If you navigate in your web browser to `http://localhost:8080/vehicles`
you should be able to see the results
