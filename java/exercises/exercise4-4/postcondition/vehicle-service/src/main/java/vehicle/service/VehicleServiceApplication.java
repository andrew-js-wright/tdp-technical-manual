package vehicle.service;

import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import vehicle.service.resource.VehicleResource;

public class VehicleServiceApplication extends Application<Configuration> {

    public static void main(String[] args) throws Exception {
        new VehicleServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "vehicle-service";
    }

    @Override
    public void initialize(Bootstrap<Configuration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(Configuration configuration,
                    Environment environment) {
        final VehicleResource resource = new VehicleResource();
        environment.jersey().register(resource);
    }
}
