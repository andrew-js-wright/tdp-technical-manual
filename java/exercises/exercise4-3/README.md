# Import the vehicle-api
Open the `build.gradle` file in the vehicle-service.

Add another dependency in the compile section which references the
api project specified as `project(':vehicle-api')`
