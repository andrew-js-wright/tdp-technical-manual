1. Open HelloWorld.java in your favourate text editor again
2. Update the file as follows:
```
    public class HelloWorld {

        private static String message = "World! Hello!";

        public static void main(String[] args) {
            System.out.println(getMessage());
        }

        private static String getMessage() {
            return message;
        }
    }
```
3. Recompile HelloWorld.java
4. Run the program
