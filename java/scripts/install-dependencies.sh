#! /bin/bash

# Install Brew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install Java
brew update
brew tap caskroom/cask
brew install brew-cask
brew cask install java

# Install Git
brew install git

# Install IntelliJ
brew cask install intellij-idea
