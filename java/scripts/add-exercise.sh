#! /bin/bash
if [ "$#" -lt 2 ]; then
    echo "Illegal number of parameters"
    echo "$0 {major-number} {minor-number} [previous-exercise]"
    echo "e.g. $0 2 1 1-6"
    exit 1;
fi

EXERCISE_PATH="exercises/exercise$1-$2"

mkdir $EXERCISE_PATH;

if [ "$#" -eq 3 ]; then
    PREVIOUS_EXERCISE_PATH="exercises/exercise$3"
else
    PREVIOUS_MINOR_VERSION=$(($2 - 1))
    PREVIOUS_MINOR_PATH="exercises/exercise$1-$PREVIOUS_MINOR_VERSION"

    if [ -d $PREVIOUS_MINOR_PATH ]
    then
        PREVIOUS_EXERCISE_PATH=$PREVIOUS_MINOR_PATH
    fi
fi

mkdir "$EXERCISE_PATH/precondition"
mkdir "$EXERCISE_PATH/postcondition"
touch "$EXERCISE_PATH/README.md"

cp -R "$PREVIOUS_EXERCISE_PATH/postcondition/" "$EXERCISE_PATH/precondition"
cp -R "$PREVIOUS_EXERCISE_PATH/postcondition/" "$EXERCISE_PATH/postcondition"

cd $EXERCISE_PATH
