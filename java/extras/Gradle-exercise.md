# Gradle

Right now we have a relatively small application (DriverApp) but already it is
becoming a pain compile and run the application. We can do it through the command-line
but it would be hard to scale across many people especially if we were to add more
external libraries and make our code available to systems as a library.

These are things that we want to do and therefore we need a better way to manage
the build process and dependencies.

Can you think of something things that might be required to know about the build process
or dependent files:
  - Version
  - Location
  - Build steps
  - Class path
  - Build output location
  - Build number
  - Build publish location

## Make gradle build your JAR
In your DriverApp directory create a file called `build.gradle`. In that enter the following line:
```
apply plugin: 'java'
```

Now run:
```
gradle build
```

That should run with no errors, lets have a look at what it's output:

```
.
├── Application.class
├── Application.java
├── build                        <----- new folder
│   ├── libs
│   │   └── DriverApp.jar
│   └── tmp
│       └── jar
│           └── MANIFEST.MF
├── build.gradle
├── driver
│   └── api
│       ├── Driver.class
│       └── Driver.java
└── libs
    └── Vehicle.jar
```

This is the equivalent of running javac followed by the jar command. 

Now lets try to run the JAR file:
```
java -jar build/libs/DriverApp.jar
```
We'll get an error which says: `no main manifest attribute, in build/libs/DriverApp.jar`

A manifest file is created by default when we create a JAR using java. The Manifest allows
us to do things like specify version numbers or make the JAR an executible. In our case it
would be convienient to make the JAR executible and to do this we need to add a `main` attribute.

We can specify this in the `gradle.build` file as follows:
```

