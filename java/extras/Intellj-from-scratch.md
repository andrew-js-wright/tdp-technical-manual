# IntelliJ

1. Find IntelliJ on you machine:
```
(cmd-space) intellij
```

2. Click `New Project`
3. Click `Gradle` on the left hand side
4. Beside `Project SDK` click `New...`
5. Click `Java JDK`
6. Make sure the file structure is open to:
```
/Library/Java/JavaVirtualMachines/jdk1.8.0_XX.jdk/Contents/Home
```
7. Click `OK`
8. In the `Addional Libraries and Frameworks` box ensure `Java` is clicked.
9. Click `Next`
10. In GroupId enter `uk.gov.training`
11. In ArtifactId enter `VehicleService`
12. Click `Next`
13. Click `Use local gradle distribution`
14. Enter the path for gradle home to be: `/usr/local/opt/gradle/libexec`
15. Select `1.8` as the Gradle JVM
16. Click OK
17. The default names should be fine, just click `Finish`
