package com.kainos.foundations.subscribers.data;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import com.kainos.foundations.subscribers.model.Subscriber;

public interface SubscriberMapper {

	@Select("SELECT name, email FROM subscribers")
	List<Subscriber> getSubscribers();
	
	@Insert("INSERT INTO subscribers (name, email) VALUES (#{name}, #{email})")
	void insertSubscriber(Subscriber subscriber);
}
