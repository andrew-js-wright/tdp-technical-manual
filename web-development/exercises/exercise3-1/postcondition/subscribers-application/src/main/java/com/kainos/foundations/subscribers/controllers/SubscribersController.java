package com.kainos.foundations.subscribers.controllers;

import io.dropwizard.views.View;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import com.kainos.foundations.subscribers.data.SubscriberMapper;
import com.kainos.foundations.subscribers.model.Subscriber;
import com.kainos.foundations.subscribers.views.Index;

@Path("/")
public class SubscribersController {
	
	private SqlSessionFactory sqlSessionFactory;

	public SubscribersController(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}
	
	@GET
	@Path("index")
	@Produces(MediaType.TEXT_HTML)
	public View index() {
		
		SqlSession session = sqlSessionFactory.openSession();
		List<Subscriber> subscribers;
		
		try {
			SubscriberMapper mapper = session.getMapper(SubscriberMapper.class);
			subscribers = mapper.getSubscribers();
		} finally {
			session.close();
		}
		
		return new Index(subscribers);
	}
	
	@POST
	@Path("addSubscriber")
	public Object addSubscriber(@FormParam("name") String name, @FormParam("email") String email) {
		
		SqlSession session = sqlSessionFactory.openSession(true);
		
		try {
			SubscriberMapper mapper = session.getMapper(SubscriberMapper.class);
			mapper.insertSubscriber(new Subscriber(name, email));
		} finally {
			session.close();
		}
		
		return Response.seeOther(UriBuilder.fromUri("/index").build()).build();
	}
}
