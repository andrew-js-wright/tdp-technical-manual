package com.kainos.foundations.subscribers;


import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.datasource.DataSourceFactory;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import com.google.common.collect.ImmutableMap;
import com.kainos.foundations.subscribers.config.SubscribersConfiguration;
import com.kainos.foundations.subscribers.controllers.SubscribersController;
import com.kainos.foundations.subscribers.data.SubscriberMapper;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.views.ViewBundle;
import io.dropwizard.assets.*;

public class SubscribersApplication extends Application<SubscribersConfiguration> {

	public static void main(String[] args) throws Exception {
		new SubscribersApplication().run(args);
	}

	@Override
    public void initialize(Bootstrap<SubscribersConfiguration> bootstrap) {        
        bootstrap.addBundle(new ViewBundle<SubscribersConfiguration>() {
	        @Override
	        public ImmutableMap<String, ImmutableMap<String, String>> getViewConfiguration(SubscribersConfiguration config) {
	            return config.getViewRendererConfiguration();
	        }
        });   
    }
	
	@Override
	public void run(SubscribersConfiguration configuration, Environment environment)
			throws Exception {
		
		
		
		/* DataSource dataSource = configuration.getDatabase().getDataSource();
		TransactionFactory transactionFactory = new JdbcTransactionFactory();
		org.apache.ibatis.mapping.Environment mybatisEnv = 
				new org.apache.ibatis.mapping.Environment("development", transactionFactory, dataSource);
		Configuration mybatisConfig = new Configuration(mybatisEnv);
		mybatisConfig.addMapper(SubscriberMapper.class);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(mybatisConfig); */
		
		// InputStream inputStream = new FileInputStream("/home/simontest1/work/subscribers-application/mybatis-config.xml");
		InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		SubscribersController subscribersController = new SubscribersController(sqlSessionFactory);
		environment.jersey().register(subscribersController);
	}

}
