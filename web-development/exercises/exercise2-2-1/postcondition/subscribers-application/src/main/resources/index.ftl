<p>Welcome to the subscribers application!</p>

<form action="addSubscriber" method="POST">
	<span>Name: </span><input type="text" name="name"></input>
	<span>Email: </span><input type="text" name="email"></input>
	<input type="submit" value="Submit" />
</form>

<br />
<br />
<p>Subscribers</p>
<br />
<table>
	<tr>
		<th>Name</th>
		<th>Email</th>
	</tr>

	<#list subscribers as subscriber>
	<tr>
		<td>${subscriber.name}</td>
		<td>${subscriber.email}</td>
	</tr>
	</#list>
</table>