package com.kainos.foundations.subscribers.views;

import java.util.List;

import com.kainos.foundations.subscribers.model.Subscriber;

import io.dropwizard.views.View;

public class Index extends View {
	
	private List<Subscriber> subscribers;
	
	public List<Subscriber> getSubscribers() {
		return subscribers;
	}

	public void setSubscribers(List<Subscriber> subscribers) {
		this.subscribers = subscribers;
	}

	public Index(List<Subscriber> subscribers) {
		super("/index.ftl");
		setSubscribers(subscribers);
	}
	
}
