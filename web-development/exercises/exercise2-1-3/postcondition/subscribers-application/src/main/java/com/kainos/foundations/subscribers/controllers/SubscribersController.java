package com.kainos.foundations.subscribers.controllers;

import io.dropwizard.views.View;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.kainos.foundations.subscribers.model.Subscriber;
import com.kainos.foundations.subscribers.views.Index;

@Path("/")
public class SubscribersController {

	private List<Subscriber> subscribers;
	
	public SubscribersController() {
		subscribers = new ArrayList<Subscriber>();
	}
	
	@GET
	@Path("index")
	@Produces(MediaType.TEXT_HTML)
	public View index() {
		return new Index(subscribers);
	}
	
	@POST
	@Path("addSubscriber")
	public View addSubscriber(@FormParam("name") String name, @FormParam("email") String email) {
		subscribers.add(new Subscriber(name, email));
		return new Index(subscribers);
	}
	
}
