package com.kainos.foundations.subscribers.controllers;

import io.dropwizard.views.View;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.kainos.foundations.subscribers.views.Index;

@Path("/")
public class SubscribersController {

	@GET
	@Path("index")
	@Produces(MediaType.TEXT_HTML)
	public View index() {
		return new Index();
	}
	
}
