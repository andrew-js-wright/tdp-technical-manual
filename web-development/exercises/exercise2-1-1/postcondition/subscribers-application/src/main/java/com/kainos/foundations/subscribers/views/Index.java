package com.kainos.foundations.subscribers.views;

import io.dropwizard.views.View;

public class Index extends View {

	public Index() {
		super("/index.ftl");
	}
	
}
