#Schedule - 4 Day testing course

This is the schedule for the testing course for new starts to Kainos in the Summer of 2015. This assumes that the class have already had 3 weeks of technical training in the following areas:

1. Databases
2. Java Programming
3. Web Development

## Faciliators
This was delivered by Andrew Wright and Simon Watson leaning heavily on the work Jonathon Holmes did in previous years and with a lot of support from Damien Coney.

Please contact them if you are planning to deliver the content and they'll be happy to help you out.

##Day 1
- 09:00 - Recap on what has been covered last week
- 09:15 - Put sheets around the room ask people in teams to go around a sheet each 
    - Write up everything you know about testing ask
    - Ask each team to come up with 3 things they'd like to learn about testing over the course, write these up and discuss how they fit in to the schedule. If you hadn't originally intended to cover something that comes us discuss how you could cover it or how it is outside the remit of the course.
- 09:30 - Why test by Rory [http://blog.pengin.pro/an-introduction-to-testing/](http://blog.pengin.pro/an-introduction-to-testing/)
- 09:50 - Pyramid of testing needs
    - Ask each person to draw a five tier pyramid
    - Fill it out on your own
    - Compare with your partner
    - Complete as a class
    - Redraw a blank pyramid
    - Point to a blank part and ask your partner to explain it to you
- 10:15 - BREAK
- 10:30 - Introduce login service
    - Recap of RESTful webservices
    - Introduce postman to hit the webservice they developed last week
    - Introduce Test Driven Development
        - Red, Green, Refactor
        - Ensure they know its optional but a useful practice to get in to
- 10:45 - Pull down [git repository](https://github.com/kainos-training/login-service)
    - Edit test class
    - Initalise the Login Resource
    - When login is attempted it should return false when the username and password are incorrect
    - Create functionality to do this
    - When login is attempted it should return true when the username and passwordare correct
    - Create functionality to do this
- 12:30 - LUNCH
- 13:30 - HTTP response request grid
    - Draw the pictures only, introduce it and let them guess
    - Get them to draw it themselves
- 14:00 - Use postman to hit newly created service identifying all of the elements of the http request/response.
    - Clients
        - Ask them to come up with 3 different clients they've all ready seen
        - The difference between a client and a server
- 14:20 - Research Jersey and implement a client to talk to the newly created service.
    - Create new client for the login service [template](https://github.com/kainos-training/login-client)
- 15:00 - Refactor login service using tdd to allow the creation of a user and password.
- 15:15 - BREAK
- 15:30 - Continue with refactor
- 16:00 - Write an integration test to test the add user functionality
- 16:45 - Update the sheets from the morning adding to what you've learnt about testing so far and highlight anything that still needs to be covered tomorrow.
- 17:00 - Cheerio

##Day 2
- 09:00 - [Crossword opener](day-two-cross-word.pdf)
- 09:30 - Dropwizard and SBT
    - Split class into two teams
    - Give them 30 mins to prepare a 5 minute presentation on DW & SBT
    - Needs to answer the following questions:
        - What is it?
        - What does it do?
        - Why is this a good thing?
- 10:15 - Break
- 10:30 - Develop out the login frontend using dropwizard & SBT - [template](https://github.com/kainos-training/bookface-frontend)
    - If someone's flying ahead they could integrate into jenkins
- 12:30 - Lunch
- 14:00 - Jake Sexton - Role of a tester on an agile team
- 14:45 - Continue development
- 15:15 - BREAK
- 16:45 - Review the sheets from yesterday, is there anything we've covered today that you wanted to learn, what knowledge have you accumulated?
- 17:00 - Cheerio

##Day 3
- 09:00 - [Wordsearch opener](day-three-wordsearch.pdf)
- 09:15 - NFR talk and JMeter workshop
    - Give NFR presentation
    - Hand out articles about big failures [MOT, Daily Mail](http://www.dailymail.co.uk/news/article-3203845/MOT-meltdown-Thousands-motorists-face-forced-road-new-government-failed-leaving-garages-unable-issue-certificates.html#ixzz3jLR65v7b), [Defra, BBC](http://www.bbc.co.uk/news/uk-31976230), [Ashley Madison](http://www.bbc.co.uk/news/technology-34044506)
    - Ask what went wrong in each case?
    - [JMeter practical](https://github.com/kainos-training/jmeter-practical)
- 10:30 - BREAK
- 10:45 - Finish off JMeter stuff
- 11:00 - Selenium
- 12:30 - LUNCH
- 13:30 - Write selenium tests and introduce page model 
- 14:00 - Deliver testing applied [presentation](Testing Applied.key)
- 15:30 - BREAK
- 15:45 - Bring in blackbox client
    - Code client
    - Import into api tests
    - Write blackbox client tests
    - Report defect
    - Import into frontends
    - Use FTL to display friends
    - Selenium test
- 17:00 - Cheerio

##Day 4
- 09:00 - [Wordsearch opener](day-four-wordsearch.pdf)
- 09:15 - Break into teams and complete bookface requirements
    - List all of your friends
    - Remove a friend
    - Unit, integration and UI test all of the above
- 12:30 - LUNCH
- 13:30 - Continue development, if people are flying on they can integrate with jenkins
- 15:00 - BREAK
- 15:15 - Chinese pictionary
- 15:45 - Work on presentation for Monday
- 17:00 - Cheerio
