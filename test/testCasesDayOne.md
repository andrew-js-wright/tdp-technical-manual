#Test Cases for login method
Tests to write for `login(String username, String password)`

1. Both username and password match the required username and password
    -> 200
2. Username and password are emtpy string
    -> 404
3. Username and password are null
    -> 404
4. Username is the required username but password is not the required password
    -> 401
5. Username is not the required username but the password is the required password
    -> 404
6. Username is the required username but in a different case and the password is the required password
    -> 204
7. Username is the required username and password is the required password in a different case
    -> 401
