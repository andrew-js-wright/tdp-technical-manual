Tuesday AM – Rory’s introductory talk to testing in Kainos/Explain requirements for the solution ‘Login' we are going to build.

Tuesday PM – Implementation of a back-end service for a simple login application (writing unit tests alongside development).

Wednesday AM – Completion of implementation of back end service and, implementing a HTTP client and using it to API test the service.
Wednesday PM – Implementation of front-end web component for login application (writing unit tests alongside development).

Thursday AM – Selenium testing front-end implementation.
Thursday PM – Presentation about testing and in depth description of the test pyramid (using examples that have been developed throughout the week). Non–functional requirements talk and JMeter workshop.

Friday AM/PM – Give the group a new requirement which uses a black-box service. Get the trainees to talk through how they think they should test this new requirement (before writing any tests).
API test this service before incorporating it into the solution. Selenium testing the new requirement.
