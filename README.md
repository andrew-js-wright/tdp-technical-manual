#Trainee Development Programme Content
This is where all of the content we use to teach our new starts at Kainos lives.

The course is designed to be delivered over 4 weeks, followed by 4 weeks of practical experience.

##Schedule
- Week one - [Databases](data/)
- Week two - [Java](java/)
- Week three - [Web Development](web-development/)
- Week four - [Testing](test/)

##Maintainers
Currently this is being maintained by Andrew Wright and Simon Watson, leaning heavily on the support of Damien Coney and Jonny Holmes

Please get in contact if you have any questions or want to deliver this training yourself.
